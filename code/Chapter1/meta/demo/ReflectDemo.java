package meta.demo;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ReflectDemo {
    public void reflect() {
        System.out.println("hello reflect!");
    }

    public void printNow() {
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        System.out.println(dateFormat.format(date));
    }

    public void printValue(String value) {
        System.out.println(value);
    }
}

