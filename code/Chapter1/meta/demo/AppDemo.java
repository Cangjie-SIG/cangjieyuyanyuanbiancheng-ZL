package meta.demo;
import java.lang.reflect.Method;

public class AppDemo {
    public static void main(String[] args) {
        try {
            //加载meta.demo.ReflectDemo类并获取实例
            Object foo = Class.forName("meta.demo.ReflectDemo").getDeclaredConstructor().newInstance();
            //遍历本类中定义的方法
            for (Method method : foo.getClass().getDeclaredMethods()) {
                //如果该方法不需要参数，就打印名称并调用
                if (method.getParameterCount() == 0) {
                    System.out.println("Call method:"+method.getName());
                    method.invoke(foo);
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
