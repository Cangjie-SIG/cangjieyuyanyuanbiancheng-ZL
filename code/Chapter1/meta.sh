#!/bin/bash
# meta program
echo '#!/bin/bash' >>$1
echo '# evenprogram' >>$1

echo "for ((i=0; i<=$2 ; i++)) do" >>$1
echo '    if [ $[ $i % 2 ] -eq 0 ]; then' >>$1
echo '        echo "$i 是偶数"' >>$1
echo '    fi' >>$1
echo 'done' >>$1

chmod +x $1