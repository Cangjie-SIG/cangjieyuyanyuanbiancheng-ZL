## 适配变更

### 适配0.55.3
### Chapter2
#### log_macro项目编译

步骤1：编译命令：
```
cjc cradle/macro_cradle.cj --output-type=dylib -o cradle.dll
```
变更为：
```
cjc --compile-macro cradle/macro_cradle.cj 
```

步骤2：编译命令：
```
cjc biz_demo.cj --macro-lib=./cradle.dll -o demo.exe
```
变更为：
```
cjc biz_demo.cj -L . -l cradle -o demo.exe
```

### Chapter11
#### macro_test项目编译
编译命令：
```
cjc log\macro_log_func.cj --output-type=dylib -o log.dll
```
变更为
```
cjc --compile-macro log\macro_log_func.cj 
```

编译命令：
```
cjc --macro-lib=.\log.dll macro_test.cj
```
变更为
```
cjc -L . -l log macro_test.cj
```

#### attr_macro项目编译
编译命令：
```
cjc attr_log\attr_macro_log_func.cj --output-type=dylib -o attr_log.dll
```
变更为
```
cjc --compile-macro attr_log\attr_macro_log_func.cj
```

编译命令：
```
cjc --macro-lib=.\attr_log.dll attr_macro_test.cj
```
变更为
```
cjc -L . -l attr_log attr_macro_test.cj
```

#### macro_use项目编译
编译命令：
```
cjc demo\first\macro_func_head.cj --output-type=dylib -o head.dll
```
变更为
```
cjc --compile-macro  demo\first\macro_func_head.cj
```

编译命令：
```
cjc demo\second\macro_log_func.cj --output-type=dylib -o log_func.dll --macro-lib=.\head.dll
```
变更为
```
cjc --compile-macro  demo\second\macro_log_func.cj -L . -l lib-macro_demo.first
```

编译命令：
```
cjc macro_use.cj --macro-lib=.\log_func.dll
```
变更为
```
cjc -L . -l lib-macro_demo.second macro_use.cj
```

#### macro_in项目编译
编译命令：
```
cjc in_call/log.cj --output-type=dylib -o log.dll
```
变更为
```
cjc --compile-macro in_call/log.cj
```

编译命令：
```
cjc macro_in_call.cj --macro-lib=.\log.dll
```
变更为
```
cjc -L . -l lib-macro_in_call macro_in_call.cj
```

### Chapter12
#### biz_demo项目编译
编译命令：
```
cjc cradle\macro_cradle_extra.cj --output-type=dylib -o cradle.dll
```
变更为
```
cjc --compile-macro cradle\macro_cradle_extra.cj
```

编译命令：
```
cjc biz_demo_extra.cj --macro-lib=.\cradle.dll
```
变更为
```
cjc -L . -l cradle biz_demo_extra.cj
```